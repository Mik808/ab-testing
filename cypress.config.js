const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    baseUrl: "https://app2.abtasty.com",
    setupNodeEvents(on, config) {
    },
    env: {
      apiUrl: "https://api.abtasty.com",
    },
  },
});
