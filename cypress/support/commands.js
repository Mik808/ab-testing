import { selectors, testData } from '../utils';

const apiUrl = Cypress.env('apiUrl');

Cypress.Commands.add('login', (sso, successfulLogin, email, pass) => {
  cy.visit('/login');

  if (sso) {
    cy.get('#GOOGLE_SIGN_IN_BUTTON iframe').then(() => {
      cy.get('div[role="button"]').click();
      // request google api, get token...
    });
  } else {
    cy.get('#email').type(email);
    cy.get('#password').type(pass);
    cy.get('button[type="submit"]').click();
    cy.intercept('POST', `${apiUrl}/api/oauth/login`).as('loginRequest');
    cy.wait('@loginRequest').then((interception) => {
      expect(interception.response.statusCode).to.equal(
        successfulLogin ? 200 : 400,
      );
    });
  }
});

Cypress.Commands.add('logout', () => {
  cy.get(testData.logOutBtn).click();
  cy.get(selectors.loginHeader).should('have.text', 'Sign in to your account');
});
