export const selectors = {
 loginFormSelector: 'form[data-gtm-form-interact-id="0"]',
 loginHeader: 'h1.FormHeader-module__title___pvb2N',
 selectorLoggedIn: '',
 logOutBtn: '',
};

export const testData = {
  email: 'test1@abtasty.com',
  password: '1234567890Wj',
  wrongEmail: 'ghjk@xxxdom.net',
  wrongPass: '1234567890Fx', 
  loginText: 'Sign in to your account',
  loggedInText: '',
};

