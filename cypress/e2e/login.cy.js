import { selectors, testData } from '../utils';

describe('Login', () => {
  it('logins with password', () => {
    cy.login(false, true, testData.email, testData.password);
    cy.get(selectors.selectorLoggedIn).should(
      'have.text',
      testData.loggedInText,
    );
    cy.logout();
  });

  it('logins with SSO', () => {
    cy.login(true, true, testData.email);
    cy.get(selectors.selectorLoggedIn).should(
      'have.text',
      testData.loggedInText,
    );
    cy.logout();
  });

  it('displays error message for wrong pass or email', () => {
    cy.login(false, false, testData.wrongEmail, testData.wrongPass);
    cy.get(
      `${selectors.loginFormSelector} .LoginForm-module__commonError___i23iO`,
    ).should('have.text', 'Please enter a valid email or password');
  });

  it('displays captcha after 3 wrong login attempts', () => {
    const loginAttempts = 3;
    const loginPromises = [];

    for (let i = 0; i < loginAttempts; i++) {
      const loginPromise = cy.login(
        false,
        false,
        testData.wrongEmail,
        testData.wrongPass,
      );
      loginPromises.push(loginPromise);
      if (i === loginAttempts - 1) {
        loginPromise.then(() => {
          cy.get('textarea#g-recaptcha-response').should('exist');
        });
      }
    }
  });

  it('show/hides password input', () => {
    cy.visit('/');
    cy.get('#password').type(testData.password);
    cy.get('svg[data-testid="showIcon"]').click();
    cy.get('#password').invoke('attr', 'type').should('eq', 'text');
    cy.get('svg[data-testid="hideIcon"]').click();
    cy.get('#password').invoke('attr', 'type').should('eq', 'password');
  });
});
