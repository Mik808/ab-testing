import { testData } from '../utils';

describe('Test password reset', () => {
  let resetLink;
  const testEmailAddress = `test.${new Date().getTime()}@${Cypress.env(
    'MAILISK_NAMESPACE',
  )}.mailisk.net`;

  it('Should login as user', () => {
    cy.login(false, true, email, password);
  });

  it('Should reset password', () => {
    cy.visit('/forgotpass');
    cy.get('#email').type(testEmailAddress);
    cy.get('form').submit();
  });

  it('Should visit reset link and set new password', () => {
    cy.mailiskSearchInbox(Cypress.env("MAILISK_NAMESPACE"), {
      to_addr_prefix: testEmailAddress,
    }).then((response) => {
      const emails = response.data;
      const email = emails[0];
      resetLink = email.text.match(/.*\[(https:\/\/abtasty.com\/.*)\].*/)[1];
      expect(resetLink).to.not.be.undefined;
    });
    cy.visit(resetLink);
    cy.get('#new-password').type('newpassword');
    cy.get('form').submit();
    // if the reset was successful we should be redirected to the login screen
    cy.location('pathname').should('eq', '/login');
  });

  it('Should login as user with new password', () => {
    cy.login(false, true, testData.email, testData.password);
  });
});
